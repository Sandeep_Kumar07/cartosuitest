package com.portico.qa.testcases;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.portico.qa.base.BaseClass;
import com.portico.qa.pages.LoginPage;
import com.portico.qa.pages.ProjectListPage;
import com.portico.qa.pages.ProjectPage;

public class CleanUp extends BaseClass{

	ProjectListPage projectListPage;
	LoginPage loginPage;
	LoginPageTest loginPageTest;
	ProjectPage projectPage;
	Logger log = Logger.getLogger(ProjectListPageTest.class);
	
	public CleanUp() throws IOException {
		super();
	}

	@BeforeMethod
	public void setUp() throws IOException {
		initialization();
		loginPage = new LoginPage();
		projectListPage = new ProjectListPage();
		projectPage = new ProjectPage();
	}
	
	@Test
	public void cleanUpProjects() throws IOException {
		loginPage.loginToApplication();
		System.out.println(projectListPage.getNumberOfProjects());
		
	}
	
	
}
