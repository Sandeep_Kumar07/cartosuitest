package com.portico.qa.testcases;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.portico.qa.base.BaseClass;
import com.portico.qa.pages.LoginPage;

public class LoginPageTest extends BaseClass {

	LoginPage loginPage;
	Logger log = Logger.getLogger(LoginPageTest.class);
	
	public LoginPageTest() throws IOException {
		super();
	}
	
	@BeforeMethod
	public void setUp() throws IOException {
		initialization();
		loginPage = new LoginPage();
	}
	
	@Test(enabled = false)
	public void loginPageTitleTest() {
//		Assert.assertEquals(loginPage.loginPageTitleTest(), "BRAND | Login", "Login page title mismatch");
		log.info("Verified login page title");
	}
	
	@Test
	public void loginTest() throws IOException {
		loginPage.loginToApplication();
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
	
}
