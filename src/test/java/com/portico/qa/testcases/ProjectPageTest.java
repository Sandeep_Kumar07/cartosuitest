package com.portico.qa.testcases;


import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.portico.qa.base.BaseClass;
import com.portico.qa.pages.LoginPage;
import com.portico.qa.pages.ProjectListPage;
import com.portico.qa.pages.ProjectPage;
import com.portico.qa.utils.ExcelDataProvider;

public class ProjectPageTest extends BaseClass{

	ProjectPage projectPage;
	ProjectListPage projectListPage;
	LoginPage loginPage;
	ExcelDataProvider excelDataProvider;
	
	public ProjectPageTest() throws IOException {
		super();
	}

	@BeforeMethod
	public void setUp() throws IOException {
		initialization();
		projectPage = new ProjectPage();
		projectListPage = new ProjectListPage();
		loginPage = new LoginPage();
		loginPage.loginToApplication();
		projectListPage.openProject();
	}
	
	@Test(enabled = true)
	public void deleteTheProject() throws IOException, InterruptedException {
		projectPage.deleteProject();
	}
	
	@Test(enabled = false)
	public void verifyUtilityBar() throws IOException {
		projectPage.clickOnCurrentState();
		projectPage.verifyUtilityBarButtons();
		projectPage.checkVisibleUtilityBarBtnsCurrent();
		projectPage.visibilityOfBtns();
		projectPage.clickOnTransitionState();
		projectPage.verifyUtilityBarButtons();
		projectPage.checkVisibleUtilityBarBtnsTransition();
		projectPage.visibilityOfBtns();
		projectPage.clickOnFutureState();
		projectPage.verifyUtilityBarButtons();
		projectPage.checkVisibleUtilityBarBtnsFuture();
		projectPage.visibilityOfBtns();
	}
	
	@DataProvider(name = "projectpropertiestestdata")
	public Object[][] getData() {
		excelDataProvider = new ExcelDataProvider();
		String excelPath = "/CartosUITest/src/main/java/com/portico/qa/testdata/ProjectNameTestData.xlsx";
		Object data[][] = excelDataProvider.testData(excelPath, "ProjectNames");
		return data;
	}
	
	@Test(dataProvider = "projectpropertiestestdata", enabled = false)
	public void saveProjectPropertiesTest(String projectName) {
		System.out.println(projectName);
		projectPage.openProjectProperties();
		Assert.assertEquals(projectPage.saveProjectProperties(projectName), "Project name already exists", "Issue: able to save duplicate project name");
	}
	
	@Test(enabled = false)
	public void verifyFleetValidation() {
		Assert.assertEquals(projectPage.clickOnFleetValidation(), "Fleet validation success", "Fleet validation failed");
	}
	
	@Test(enabled = false)
	public void importDCA() {
		projectPage.clickOnImportAssetsBtn();
	}
	
	@Test(enabled = false)
	public void importDSM() throws InterruptedException {
		projectPage.clickOnImportDsm();
	}
	
	@Test(enabled = false)
	public void importFloorMap() {
		projectPage.importfloorMapJpg();
	}
	
	
	
	
	
	
	
//	@AfterMethod
//	public void tearDown() {
//		driver.quit();
//	}
	
	
}
