package com.portico.qa.testcases;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.portico.qa.base.BaseClass;
import com.portico.qa.pages.LoginPage;
import com.portico.qa.pages.ProjectListPage;
import com.portico.qa.pages.ProjectPage;

public class ProjectListPageTest extends BaseClass {
	
	ProjectListPage projectListPage;
	LoginPage loginPage;
	LoginPageTest loginPageTest;
	ProjectPage projectPage;
	Logger log = Logger.getLogger(ProjectListPageTest.class);
	
	public ProjectListPageTest() throws IOException {
		super();
	}
	
	@BeforeMethod
	public void setUp() throws IOException {
		initialization();
		loginPage = new LoginPage();
		projectListPage = new ProjectListPage();
		projectPage = new ProjectPage();
		loginPage.loginToApplication();
	}
	
	@Test(enabled = false)
	public void createFolderTest() throws IOException {
		projectListPage.createFolder("Test Folder");
	}
	
	@Test(enabled = false)
	public void createProjectTest() throws IOException {
		log.info("Test cases started");
		projectListPage.createProject("Test Project");
		log.info("Creating project.");
		projectPage.waitTillProjectViewIsLoaded();
		Assert.assertEquals(driver.getTitle(), "Portico | Project", "Project Title mismatch");
		driver.navigate().back();
	}
	
	@Test(enabled = false)
	public void projectListToolBarBtnsTest() {
		projectListPage.selectProject();
		projectListPage.getProjectListToolBarBtns();
		projectListPage.checkProjectListToolBarBtns();
		projectListPage.verifyProjectListToolBarBtns();
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
}
