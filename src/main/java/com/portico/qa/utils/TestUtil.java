package com.portico.qa.utils;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.portico.qa.base.BaseClass;

public class TestUtil extends BaseClass{

	public TestUtil() throws IOException {
		super();
	}

	WebDriverWait wait = new WebDriverWait(driver, 30);
	Actions builder = new Actions(driver);
	public static long PAGE_LOAD_TIMEOUT = 500;
	public static long IMPLICIT_WAIT = 20;
	
	public void acceptAlert() {
		try {
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public boolean isAlertPresent() {
		if(wait.until(ExpectedConditions.alertIsPresent())!=null){
			return true;
		}
		else {
			return false;
		}
	}
	
	public void waitTillElementIsClickable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public void waitTillElementIsVisible(By element) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(element));
	}
	
	public void waitTillElementIsInvisible(WebElement element) {
		wait.until(ExpectedConditions.invisibilityOf(element));
	}
	
	public void doubleClickOnElement(WebElement element) {
		builder.moveToElement(element).doubleClick().perform();
	}
	
	public void clickOnElement(WebElement element) {
		builder.moveToElement(element).click().perform();
	}
	
	public void showListItems(List listName) {
		Iterator itr = listName.iterator();
		while(itr.hasNext()) {
			 Object element = itr.next();
	         System.out.println(element);
		}
		
	}
	
	
	
	
}
