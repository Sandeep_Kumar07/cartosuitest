package com.portico.qa.utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {

	static XSSFWorkbook workbook;
	static XSSFSheet sheet;
	
	public ExcelUtils(String excelPath, String sheetName) {
		
		try {
			workbook = new XSSFWorkbook(excelPath);
			sheet = workbook.getSheet(sheetName);
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
	}
	

	public static int getRowCount() {
	
		int rowCount =0;
		try {
			rowCount = sheet.getPhysicalNumberOfRows();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return rowCount;
	}
	
	public static int getColCount() {
		int colCount = 0;
		try {
			colCount = sheet.getRow(0).getPhysicalNumberOfCells();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return colCount;
	}
	
	public static String getStringCellData(int rowNum, int colNum) {
		String cellData = null;
		try {
			cellData = sheet.getRow(rowNum).getCell(colNum).getStringCellValue();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		return cellData;
	}
	
	public static void getIntCellData(int rowNum, int colNum) {
		try {
			double cellData = sheet.getRow(rowNum).getCell(colNum).getNumericCellValue();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	
	
	
}
