package com.portico.qa.utils;

import org.testng.annotations.DataProvider;

public class ExcelDataProvider {

	@DataProvider(name = "projectpropertiestestdata")
	public Object[][] getData() {
		String excelPath = "/CartosUITest/src/main/java/com/portico/qa/testdata/ProjectNameTestData.xlsx";
		Object data[][] = testData(excelPath, "ProjectNames");
		return data;
	}
	
	
	public static Object[][] testData(String excelPath, String sheetName) {
		ExcelUtils excel = new ExcelUtils(excelPath, sheetName);
		int rowCount = excel.getRowCount();
		int colCount = excel.getColCount();
		Object data[][]= new Object[rowCount-1][colCount];;
		try {
			for(int i=1; i<rowCount; i++) {
				for(int j=0; j<colCount; j++) {
					String CellData= excel.getStringCellData(i, j);
					data[i-1][j]= CellData;
				}
  			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	
	
	
	
}
