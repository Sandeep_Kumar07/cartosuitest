package com.portico.qa.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.portico.qa.base.BaseClass;
import com.portico.qa.testdata.ProjectListPageTestData;
import com.portico.qa.utils.TestUtil;

public class ProjectListPage extends BaseClass{
	
	TestUtil util= new TestUtil();
	ProjectPage projectPage = new ProjectPage();
	ProjectListPageTestData projectListPageTestData = new ProjectListPageTestData();
	Logger log = Logger.getLogger(BaseClass.class);
	List<String> projectListToolBarBtnsDisplayed = new ArrayList<String>();
	boolean check;

	@FindBy (xpath = "//a[@id = 'createFolder']")
	WebElement createFolder;
	
	@FindBy (xpath = "//input[@id = 'folderName']")
	WebElement folderName;
	
	@FindBy (xpath = "//button[@id = 'btnCreatefolder']")
	WebElement createFolderBtn;
	
	@FindBy (xpath = "//a[@id = 'createProject2']")
	WebElement createProjectBtn;
	
	@FindBy (xpath = "//input[@id = 'projectName']")
	WebElement projectNameInputField;
	
	@FindBy (xpath = "//button[@id = 'btnCreateProject']")
	WebElement btnCreateProject;
	
	@FindBy (xpath = "//td[text()= 'Test Project']")
	WebElement projectName;
	
	@FindBy(xpath = "//table[@class = 'table pointer ui-droppable']")
	WebElement tableOfProjectList;
	
	@FindBy(how = How.XPATH, using = "//div[@class = 'panel-heading homeHeaderLinks']//a")
	List<WebElement> projectListToolBarBtns;
	
	@FindBy(how = How.XPATH , using = "//*[@class = 'table pointer ui-droppable']/tbody/tr[2]")
	List<WebElement> rowsOfProjectList;
	
	public ProjectListPage() throws IOException{
		PageFactory.initElements(driver, this);
	}
	
	public String projectListPageTitleTest() {
		return driver.getTitle();
	}
	
	public void createFolder(String nameOfFolder) throws IOException {
		util.waitTillElementIsClickable(createFolder);
		createFolder.click();
		folderName.sendKeys(nameOfFolder);
		createFolderBtn.click();
	}
	
	public void createProject(String nameOfProject) throws IOException {
		util.waitTillElementIsClickable(createProjectBtn);
		createProjectBtn.click();
		util.waitTillElementIsClickable(projectNameInputField);
		projectNameInputField.sendKeys(nameOfProject);
		btnCreateProject.click();
	}
	
	public void selectProject() {
		util.clickOnElement(projectName);
	}
	
	public void openProject() {
		util.doubleClickOnElement(projectName);
		projectPage.waitTillProjectViewIsLoaded();
		log.info("Opening project.");
	}
	
	public int getNumberOfProjects() {
		return rowsOfProjectList.size();
	}
	
	public void getProjectListToolBarBtns() {
		for(int i=0; i<projectListToolBarBtns.size(); i++) {
			if(projectListToolBarBtns.get(i).isDisplayed()) {
				projectListToolBarBtnsDisplayed.add(projectListToolBarBtns.get(i).getAttribute("title"));
			}
		}
	}
	
	public boolean checkProjectListToolBarBtns() {
		if(driver.getCurrentUrl().contains("portico")) {
			check = projectListToolBarBtnsDisplayed.equals(projectListPageTestData.porticoProjectListPageToolBarBtns());
			projectListToolBarBtnsDisplayed.clear();
		}
		else {
			projectListToolBarBtnsDisplayed.remove(projectListToolBarBtnsDisplayed.size()-2);
			check = projectListToolBarBtnsDisplayed.equals(projectListPageTestData.cartosProjectListPageToolBarBtns());
			projectListToolBarBtnsDisplayed.clear();
		}
		return check;
	}
	
	public void verifyProjectListToolBarBtns() {
		if(check == true) {
			System.out.println("All the project list page toolbar options are visible and no extra options available");
		}
		else {
			System.out.println("Mismatch in visible toolbar options");
		}
	}
}
