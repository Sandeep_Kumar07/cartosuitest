package com.portico.qa.pages;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.portico.qa.base.BaseClass;
import com.portico.qa.testdata.ProjectPageUtilityBarTestData;
import com.portico.qa.utils.TestUtil;

public class ProjectPage extends BaseClass{

	TestUtil util= new TestUtil();
	boolean check;
	Logger log = Logger.getLogger(BaseClass.class);
	ProjectPageUtilityBarTestData utilityBarTestData= new ProjectPageUtilityBarTestData();
	List<String> btnsInUtilityBarDisplayed = new ArrayList<String>();
	
	@FindBy(id = "projectBreCrumNav")
	WebElement breadCrumbLevelProject;
	
	@FindBy(id = "btnDeleteLevel")
	WebElement deleteProjectButton;
	
	@FindBy(xpath = "//img[@src = '/img/loader.GIF']")
	WebElement loaderImg;
	
	By loaderImg1 = By.xpath("//img[@src = '/img/loader.GIF']");
	
	@FindBy(xpath = "//span[@title = 'Switch View']")
	WebElement switchViewBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@id= 'toolbar']/div//button")
	List<WebElement> utilityBarBtns;
	
	@FindBy(xpath = "//a[@id = 'currentState']")
	WebElement currentStateBtn;
	
	@FindBy(id = "transitionState")
	WebElement transitionStateBtn;
	
	@FindBy(id = "futureState")
	WebElement futureStateBtn;
	
	@FindBy(xpath = "//div[@class= 'toast-message']")
	WebElement toastMessage;
	
	@FindBy(xpath = "//input[@name= 'floorName']")
	WebElement projectNameInProjectProperties;
	
	@FindBy(xpath = "//button[@id= 'btnSaveProperty']")
	WebElement saveProjectPropertiesBtn;
	
	@FindBy(xpath = "//button[@id='btnFloorMap']")
	WebElement importFloorMapBtn;
	
	@FindBy(xpath = "//input[@id = 'floorMapUploadInput']")
	WebElement chooseImportFloorMapFile;
	
	@FindBy(xpath = "//button[@id= 'btnFleetValidation']")
	WebElement fleetValidationBtn;
	
	@FindBy(xpath = "//button[@id = 'btnImport']")
	WebElement importBtn;
	
	@FindBy(xpath = "//a[@id = 'linkImportAsset']")
	WebElement importAssetsBtnInDropdown;
	
	@FindBy(xpath = "//button[@id = 'btnFloorMapUpload']")
	WebElement uploadFloorMapBtn;
	
	@FindBy(xpath = "//input[@id = 'file-input']")
	WebElement chooseInputDcaFile;
	
	@FindBy(xpath = "//button[@id = 'btn-asset-import']")
	WebElement uploadDcaFileBtn;
	
	@FindBy(xpath = "//button[@id = 'btnColumnMapping']")
	WebElement importDcaFileBtn;
	
	@FindBy(xpath = "//a[@id = 'linkImportAsm']")
	WebElement importDsmBtnInDropdown;
	
	@FindBy(xpath = "//div[@id = 'file-input-container']/input")
	WebElement chooseInputDsmFile;
	
	@FindBy(xpath = "//button[@id = 'btn-dsm-import']")
	WebElement importDsmBtnInDsmPopup;
	
	
	public ProjectPage() throws IOException{
		PageFactory.initElements(driver, this);
	}
	
	public void deleteProject() throws InterruptedException {
		currentStateBtn.click();
		util.clickOnElement(breadCrumbLevelProject);
		util.waitTillElementIsClickable(deleteProjectButton);
		deleteProjectButton.click();
		util.acceptAlert();
		log.info("Deleting project.");
	}
	
	public void waitTillProjectViewIsLoaded() {
		if(driver.getCurrentUrl().contains("portico")) {
			util.waitTillElementIsClickable(switchViewBtn);
		}
		else {
			try {
				util.waitTillElementIsVisible(loaderImg1);
				util.waitTillElementIsInvisible(loaderImg);
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void clickOnCurrentState() {
		util.waitTillElementIsClickable(currentStateBtn);
		currentStateBtn.click();
	}
	
	public void clickOnTransitionState() {
		transitionStateBtn.click();
	}
	
	public void clickOnFutureState() {
		futureStateBtn.click();
	}
	
	public void verifyUtilityBarButtons() {
		for (int i =0; i<utilityBarBtns.size(); i++) {
			if(utilityBarBtns.get(i).isDisplayed()) {
				btnsInUtilityBarDisplayed.add(utilityBarBtns.get(i).getAttribute("id"));
			}
		}
		btnsInUtilityBarDisplayed.remove(btnsInUtilityBarDisplayed.size()-1);
	}
	
	public boolean checkVisibleUtilityBarBtnsCurrent() {
		if(driver.getCurrentUrl().contains("portico")) {
			check = btnsInUtilityBarDisplayed.equals(utilityBarTestData.porticoCurrentStateVisibleUtilityBarBtns());
			btnsInUtilityBarDisplayed.clear();
		}
		else {
			check = btnsInUtilityBarDisplayed.equals(utilityBarTestData.cartosCurrentStateVisibleUtilityBarBtns());
			btnsInUtilityBarDisplayed.clear();
		}
		return check;
	}

	public boolean checkVisibleUtilityBarBtnsTransition() {
		if(driver.getCurrentUrl().contains("portico")) {
			check = btnsInUtilityBarDisplayed.equals(utilityBarTestData.porticoTransitionStateVisibleUtilityBarBtns());
			btnsInUtilityBarDisplayed.clear();
		}
		else {
			check = btnsInUtilityBarDisplayed.equals(utilityBarTestData.cartosTransitionStateVisibleUtilityBarBtns());
			btnsInUtilityBarDisplayed.clear();
		}
		return check;
	}
	
	public boolean checkVisibleUtilityBarBtnsFuture() {
		if(driver.getCurrentUrl().contains("portico")) {
			check = btnsInUtilityBarDisplayed.equals(utilityBarTestData.porticoFutureStateVisibleUtilityBarBtns());
			btnsInUtilityBarDisplayed.clear();
		}
		else {
			check = btnsInUtilityBarDisplayed.equals(utilityBarTestData.cartosFutureStateVisibleUtilityBarBtns());
			btnsInUtilityBarDisplayed.clear();
		}
		return check;
	}
	
	public void visibilityOfBtns() {
		if (check == true) {
			System.out.println("All the toolbar options are visible and no extra options available");
		}
		else {
			System.out.println("Mismatch in visible toolbar options");
		}
	}

	public String saveProjectProperties(String projectName) {
		projectNameInProjectProperties.clear();
		projectNameInProjectProperties.sendKeys(projectName);
		saveProjectPropertiesBtn.click();
		util.waitTillElementIsClickable(toastMessage);
		return toastMessage.getText();
	}

	public void openProjectProperties() {
		util.clickOnElement(breadCrumbLevelProject);
	}

	public String clickOnFleetValidation() {
		fleetValidationBtn.click();
		util.waitTillElementIsClickable(toastMessage);
		return toastMessage.getText();
	}

	public void clickOnImportAssetsBtn() {
		importBtn.click();
		importAssetsBtnInDropdown.click();
		String filePath = "TestFiles/10 assets DCA file.csv";
		chooseInputDcaFile.sendKeys(getFilePath(filePath));
		System.out.println(getFilePath(filePath));
		uploadDcaFileBtn.click();
		importDcaFileBtn.click();
	}
	
	public void clickOnImportDsm() throws InterruptedException {
		importBtn.click();
		importDsmBtnInDropdown.click();
		util.waitTillElementIsClickable(chooseInputDsmFile);
		String filePath = "C:\\Users\\kumar.sandeep\\Desktop\\Desktop1.0\\DSM.csv";
		chooseInputDsmFile.sendKeys(filePath);
		importDsmBtnInDsmPopup.click();
		if(util.isAlertPresent()) {
			util.acceptAlert();
		}
	}

	public String getFilePath(String filePath) {
		return new File(filePath).getAbsolutePath();
	}

	public void importfloorMapJpg() {
		importFloorMapBtn.click();
		util.waitTillElementIsClickable(chooseImportFloorMapFile);
		String filePath = "TestFiles/1750 Vine Hollywood-T-1 copier.pdf";
		chooseImportFloorMapFile.sendKeys(getFilePath(filePath));
		System.out.println(getFilePath(filePath));
		uploadFloorMapBtn.click();
		
	}





}
