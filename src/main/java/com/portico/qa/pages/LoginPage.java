package com.portico.qa.pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.portico.qa.base.BaseClass;
import com.portico.qa.utils.TestUtil;

public class LoginPage extends BaseClass{

	TestUtil util= new TestUtil();
	Logger log = Logger.getLogger(LoginPage.class);
	
	@FindBy(xpath = "//input[@id ='userName']")
	WebElement userName;
	
	@FindBy(xpath = "//input[@id ='inputPassword']")
	WebElement password;
	
	@FindBy(xpath = "//input[@id = 'submit']")
	WebElement submitBtn;
	
	public LoginPage() throws IOException{
		PageFactory.initElements(driver, this);
	}

	public String loginPageTitleTest() {
		return driver.getTitle();
	}
	
	public ProjectListPage loginToApplication() throws IOException {
		util.acceptAlert();
		userName.sendKeys(prop.getProperty("userName"));
		log.info("Entered username");
		password.sendKeys(prop.getProperty("password"));
		log.info("Entered password");
		submitBtn.click();
		log.info("Logging in");
//		Assert.assertEquals(driver.getTitle(), "Portico | Projects", "Project List page title mismatch");
		return new ProjectListPage();
	}
	
	
	
	
	
	
}
