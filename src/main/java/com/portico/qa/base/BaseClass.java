package com.portico.qa.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.portico.qa.utils.TestUtil;


public class BaseClass {

	public static WebDriver driver;
	public static Properties prop;
	
	public BaseClass() throws IOException {
		
		prop = new Properties();
		FileInputStream input = new FileInputStream("../CartosUITest/src/main/java/com/portico/config/config.properties");
		prop.load(input);
		
	}
	
	public static void initialization() {
		
		if(prop.getProperty("browser").equals("chrome")) {
			Logger log = Logger.getLogger(BaseClass.class);
			log.info("Initiating webdriver");
			System.setProperty("webdriver.chrome.driver", "../CartosUITest/chromeDriver/chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
//		driver.get(prop.getProperty("cartosurl"));
		driver.get(prop.getProperty("porticourl"));
	}
	
	
}
