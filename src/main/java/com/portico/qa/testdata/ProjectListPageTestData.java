package com.portico.qa.testdata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.portico.qa.base.BaseClass;

public class ProjectListPageTestData extends BaseClass{

	public ProjectListPageTestData() throws IOException {
		super();
	}

	public List porticoProjectListPageToolBarBtns() {
		List<String> projectListPageToolBarBtns = new ArrayList<String>(){{
			add("Create New Folder");
			add("Create new project");
			add("Tile View");
			add("List View");
			add("Copy Project");
			add("Grant User Access");
			add("Export Project");
			add("Import Project");
			add("Archive Project");
			
		}};
		return projectListPageToolBarBtns;
	}
	
	public List cartosProjectListPageToolBarBtns() {
		List<String> projectListPageToolBarBtns = new ArrayList<String>(){{
			add("Create New Folder");
			add("Create new project");
			add("Tile View");
			add("List View");
			add("Copy Project");
			add("Grant User Access");
			add("Export Project");
			add("Import Project");
			add("Archive Project");
			add("Share Project");
			
		}};
		return projectListPageToolBarBtns;
	}
	
	
}
