package com.portico.qa.testdata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.portico.qa.base.BaseClass;
import com.portico.qa.utils.TestUtil;

public class ProjectPageUtilityBarTestData extends BaseClass{
	
	TestUtil util = new TestUtil();

	public ProjectPageUtilityBarTestData() throws IOException {
		super();
	}

	public List porticoCurrentStateVisibleUtilityBarBtns() {
	List<String> UtilityBarBtns = new ArrayList<String>() {{
		
		add("btnImport");
		add("btnExport");
		add("btnToggleLabels");
		add("btnToggleSubIcons");
		add("btnToggleMapId");
		add("btnUndo");
		add("btnRedo");
		add("btnZoomIn");
		add("btnZoomOut");
		add("btnZoomSlider");
		add("btnIconZoom");
		add("btnLabelSetting");
		add("btnBasicStats");
		add("btnViewDsm");
		add("btnViewBog");
		add("btnPageVolume");
		add("btnColorZone");
		add("btnScaleMap");
		add("btnFleetValidation");
		
	}};
	return UtilityBarBtns;
}
	
	public List cartosCurrentStateVisibleUtilityBarBtns() {
	List<String> UtilityBarBtns = new ArrayList<String>() {{
		
		add("btnImport");
		add("btnExport");
		add("btnToggleLabels");
		add("btnToggleSubIcons");
		add("btnToggleMapId");
		add("btnUndo");
		add("btnRedo");
		add("btnZoomIn");
		add("btnZoomOut");
		add("btnZoomSlider");
		add("btnIconZoom");
		add("btnLabelSetting");
		add("btnResellerPricingCatalog");
		add("btnBasicStats");
		add("btnViewDsm");
		add("btnPageVolume");
		add("btnColorZone");
		add("btnScaleMap");
		add("btnFleetValidation");
		
	}};
	return UtilityBarBtns;
}
	
	public List porticoTransitionStateVisibleUtilityBarBtns() {
	List<String> UtilityBarBtns = new ArrayList<String>() {{
		
		add("btnImport");
		add("btnExport");
		add("btnToggleLabels");
		add("btnToggleSubIcons");
		add("btnToggleMapId");
		add("btnUndo");
		add("btnRedo");
		add("btnZoomIn");
		add("btnZoomOut");
		add("btnZoomSlider");
		add("btnIconZoom");
		add("btnLabelSetting");
		add("btnBasicStats");
		add("btnViewDsm");
		add("btnViewBog");
		add("btnPageVolume");
		add("btnColorZone");
		add("btnScaleMap");
		add("btnFleetValidation");
		
	}};
	return UtilityBarBtns;
}
	
	public List cartosTransitionStateVisibleUtilityBarBtns() {
	List<String> UtilityBarBtns = new ArrayList<String>() {{
		
		add("btnImport");
		add("btnExport");
		add("btnToggleLabels");
		add("btnToggleSubIcons");
		add("btnToggleMapId");
		add("btnUndo");
		add("btnRedo");
		add("btnZoomIn");
		add("btnZoomOut");
		add("btnZoomSlider");
		add("btnIconZoom");
		add("btnLabelSetting");
		add("btnResellerPricingCatalog");
		add("btnBasicStats");
		add("btnViewDsm");
		add("btnPageVolume");
		add("btnColorZone");
		add("btnScaleMap");
		add("btnFleetValidation");
		add("btnGridView");
		
	}};
	return UtilityBarBtns;
}
	
	public List porticoFutureStateVisibleUtilityBarBtns() {
	List<String> UtilityBarBtns = new ArrayList<String>() {{
		
		add("btnImport");
		add("btnExport");
		add("btnToggleLabels");
		add("btnToggleSubIcons");
		add("btnToggleMapId");
		add("btnDisposition");
		add("btnUndo");
		add("btnRedo");
		add("btnZoomIn");
		add("btnZoomOut");
		add("btnZoomSlider");
		add("btnIconZoom");
		add("btnLabelSetting");
		add("btnBasicStats");
		add("btnViewDsm");
		add("btnViewBog");
		add("btnPageVolume");
		add("btnColorZone");
		add("btnScaleMap");
		add("btnFleetValidation");
		
	}};
	return UtilityBarBtns;
}

	public List cartosFutureStateVisibleUtilityBarBtns() {
	List<String> UtilityBarBtns = new ArrayList<String>() {{
		
		add("btnImport");
		add("btnExport");
		add("btnToggleLabels");
		add("btnToggleSubIcons");
		add("btnToggleMapId");
		add("btnDisposition");
		add("btnUndo");
		add("btnRedo");
		add("btnZoomIn");
		add("btnZoomOut");
		add("btnZoomSlider");
		add("btnIconZoom");
		add("btnLabelSetting");
		add("btnResellerPricingCatalog");
		add("btnBasicStats");
		add("btnViewDsm");
		add("btnPageVolume");
		add("btnColorZone");
		add("btnScaleMap");
		add("btnFleetValidation");
		
	}};
	return UtilityBarBtns;
}
}
